FROM dtzar/helm-kubectl as k8s

FROM civo/cli:latest as civo 

FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest as final
COPY --from=civo /usr/local/bin/civo /usr/local/bin/civo 
COPY --from=k8s /usr/local/bin/kubectl /usr/local/bin/kubectl 
