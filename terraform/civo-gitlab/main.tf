locals {
  parse_config = yamldecode(data.local_file.kubeconfig.content)
  kubernetes_ca_cert = base64decode(local.parse_config.clusters[0].cluster.certificate-authority-data)
  kubernetes_api_url = local.parse_config.clusters[0].cluster.server
}


data "local_file" "kubeconfig" {
  filename          =  var.kubeconfig_path
}


provider "kubernetes" {
  config_path    =  var.kubeconfig_path
}

resource "kubernetes_cluster_role_binding" "gitlab" {
  depends_on = [kubernetes_service_account.ci]
  metadata {
    name = "gitlab"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab"
    namespace = "kube-system"
  }

}
resource "kubernetes_service_account" "ci" {
  metadata {
    name = "gitlab"
    namespace = "kube-system"
  }
}

data "kubernetes_secret" "ci" {
  metadata {
    name = kubernetes_service_account.ci.default_secret_name
    namespace = "kube-system"
  }
}

resource "gitlab_project_cluster" "civo" {
  project                       = var.gitlab_project_id
  name                          = "civo-${var.cluster_name}"
  domain                        = var.dns_entry
  enabled                       = true
  kubernetes_api_url            = local.kubernetes_api_url
  kubernetes_token              = lookup(data.kubernetes_secret.ci.data, "token")
  kubernetes_ca_cert            = local.kubernetes_ca_cert
  environment_scope             = "*"
}
