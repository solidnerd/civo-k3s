variable "write_kubeconfig" {
  default = true
}

variable "gitlab_project_id" {
  type = string
  description = "(optional) describe your variable"
  default = "28366099"
}

variable "cluster_name" {
  type = string
  description = "(optional) describe your variable"
}

variable "dns_entry" {
  type = string
  description = "(optional) describe your variable"
}

variable "kubeconfig_path" {
  type = string
  default = "~/.kube/config"
}
