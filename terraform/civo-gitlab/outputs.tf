// output "gitlab_service_account_token" {
//   value = "${lookup(data.kubernetes_secret.ci.data, "token")}"
// }

output "ca_cert" {
  value = local.kubernetes_ca_cert
}

output "api_url" {
  value = local.kubernetes_api_url
}
