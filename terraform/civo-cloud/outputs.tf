
output "cluster_name" {
  value = random_pet.cluster_name.id
}

output "dns_entry" {
  value = civo_kubernetes_cluster.cluster.dns_entry
}
