# civo-k3s

This repository runs a GitLab Ci pipeline to create a k3s cluster with 3 nodes on [Civo](Civo.com) with a random pet name (example: causal-pony).

## Access Remote State

```shell
export CIVO_TOKEN=0KPzbRmks0MhyTe6F7cGBlA4uCpJwYd5tnDOUXgroqEivaZVSj
export GITLAB_PROJECT_ID=28366099
export GITLAB_USERNAME=solidnerd
export GITLAB_TOKEN=6KaS1WyhAUrwQ3frVf2h
export TF_STATE_NAME=civo-k3s
```

```shell
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${TF_STATE_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock" \
    -backend-config="username=${GITLAB_USERNAME}" \
    -backend-config="password=${GITLAB_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

### Civo Credentials

Please create a variable called `CIVO_TOKEN` in your GitLab project settings with your Civo token.

* [Add a variable to a project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)

### GitLab CI

Please rename `example_gitlab-ci.yml` to `.gitlab-ci.yml`.

### Kubeconfig

Not part of this workflow. You can manually download your `kubeconfig` via the Civo GUI.
## Pipeline execution

You have to manually execute the stage `apply` in your GitLab -> CI/CD -> Pipelines menu.

Just remove the `when` section in the `apply` stage to have a fully automated pipeline:

```yaml
  when: manual
```

## Nodes

By default Civo deployes your k3s cluster with 3 nodes.

If you want more or less nodes, simply add this line to the `civo_kubernetes_cluster` resource section in `main.tf`:

```terrafrom
num_target_nodes = 4
```


## Documenation

[Terrafrom provider documentation](https://registry.terraform.io/providers/civo/civo/latest/docs)



## 


touch .terraform.rc
export TF_CLI_CONFIG_FILE=$(pwd)/.terraformrc
